import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  rutas = [
    {
      name: 'HOME',
      path: '/home'
    },
    {
      name: 'ABOUT',
      path: '/about'
    },
    {
      name: 'CONTACT',
      path: '/contact'
    },
    {
      name: 'POSTS',
      path: '/posts'
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
